const { fighter } = require("../models/fighter");

function isFields(data) {
  let flag = true;
  let dataKeys = Object.keys(data);
  let fighterKeys = Object.keys(fighter);
  if (dataKeys.length > fighterKeys.length - 1 || dataKeys.includes("id"))
    return false;
  dataKeys.forEach((item) => {
    if (!fighterKeys.includes(item)) flag = false;
  });
  if (dataKeys.length <= fighterKeys.length - 1 && flag) return true;
  else return false;
}
function isPowerValid(power) {
  if (power < 100 && power>0) return true;
  return false;
}
function isDefenseValid(defense) {
  if (defense > 0 && defense < 10) return true;
  return false;
}

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  if (req.method !== "POST") next();
  else {
    const data = req.body;
    try {
      let isValid =
        isFields(data) &&
        isPowerValid(data.power) &&
        isDefenseValid(data.defense) &&
        data.name;
      if (isValid) {
        res.status(200);
        next();
      } else throw new Error("data for create fighter is  not valid");
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  if (req.method !== "PUT") next();
  else {
    const data = req.body;
    try {
      let isValid =
        isFields(data) &&
        isPowerValid(data.power) &&
        isDefenseValid(data.defense);
      if (isValid) {
        res.status(200);
        next();
      } else throw new Error("data for update fighter is  not valid");
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
