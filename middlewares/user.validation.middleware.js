const { user } = require("../models/user");

function isFields(data) {
  let flag = true;
  let dataKeys = Object.keys(data);
  let userKeys = Object.keys(user);
  if (dataKeys.length > userKeys.length - 1 || dataKeys.includes("id")) return false;
  if (!data.firstName || !data.lastName)  return false;
  dataKeys.forEach((item) => {
    if (!userKeys.includes(item)) flag = false;
  });
  if (dataKeys.length <= userKeys.length - 1 && flag) return true;
  else return false;
}
function isEmailValid(email) {
  let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@gmail.com$/;
  return re.test(String(email).toLowerCase());
}

function isPhoneValid(phone) {
  let re = /^\+380\d{3}\d{2}\d{2}\d{2}$/;
  return re.test(String(phone).toLowerCase());
}

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  if (req.method !== "POST") next();
  else {
    const data = req.body;
    try {
      let isValid =
        isFields(data) &&
        isEmailValid(data.email) &&
        isPhoneValid(data.phoneNumber);
      if (isValid) {
        res.status(200);
        next();
      } else throw new Error("data for create user is  not valid");
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  if (req.method !== "PUT") next();
  else {
    const data = req.body;
    try {
      let isValid =
        isFields(data) &&
        isEmailValid(data.email) &&
        isPhoneValid(data.phoneNumber);
      if (isValid) {
        res.status(200);
        next();
      } else throw new Error("data for update user is  not valid");
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
