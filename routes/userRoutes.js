const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");

const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user

router.use("/", createUserValid);
router.use("/:id", updateUserValid);

router.get("/", function (req, res) {
  try {
    const data = UserService.getAll();
    if (data) res.send(data);
    else throw new Error("users not found");
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.get("/:id", function (req, res) {
  try {
    const id = req.params.id;
    const data = UserService.getOne({ id: id });
    if (data) res.status(200).send(data);
    else throw new Error("User not found");
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.post("/", function (req, res) {
  try {
    const data = UserService.createUser(req.body);
    if (data) res.send(data);
    else throw new Error("User not created");
  } catch (e) {
    res.send(e.message);
  }
});

router.put("/:id", function (req, res) {
  try {
    const id = req.params.id;
    //const data = UserService.getOne({ id: id });
    let { firstName, lastName, email, phoneNumber, password } = req.body;
    const dataToUpdate = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phoneNumber: phoneNumber,
      password: password,
    };
    const data = UserService.updateUser(id, dataToUpdate);
    if (data) res.status(200).send(UserService.getOne({ id: id }));
    else throw new Error("User not updated");
  } catch (e) {
    res.status(400).send(e.message);
  }
});

router.delete("/:id", function (req, res) {
  try {
    const id = req.params.id;
    const user = UserService.delete(id);
    if (user[0]) {
      let { firstName, lastName } = user[0];
      res.send(firstName + " " + lastName + " delete successful");
    } else throw new Error("User not deleted");
  } catch (e) {
    res.send(e.message);
  }
});

module.exports = router;
