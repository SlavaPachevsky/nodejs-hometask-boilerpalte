const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter
router.use("/", createFighterValid);
router.use("/:id", updateFighterValid);

router.get("/", function (req, res) {
  try {
    const data = FighterService.getAll();
    if (data) res.send(data);
    else throw new Error("Fighter not found");
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.get("/:id", function (req, res) {
  try {
    const id = req.params.id;
    const data = FighterService.getOne({ id: id });
    if (data) res.send(data);
    else throw new Error("Fighter not found");
  } catch (e) {
    res.status(404).send(e.message);
  }
});

router.post("/", function (req, res) {
  try {
    const data = FighterService.createUser(req.body);
    if (data) res.send(data);
    else throw new Error("Fighter not create");
  } catch (e) {
    res.send(e.message);
  }
});

router.put("/:id", function (req, res) {
  try {
    const id = req.params.id;
    //const data = FighterService.getOne({ id: id })
    let { name, health, power, defense } = req.body;
    const dataToUpdate = {
      name: name,
      health: health,
      power: power,
      defense: defense,
    };
    const data = FighterService.updateUser(id, dataToUpdate);
    if (data) res.status(200).send(FighterService.getOne({ id: id }));
    else throw new Error("Fighter not updated");
  } catch (e) {
    res.status(400).send(e.message);
  }
});

router.delete("/:id", function (req, res) {
  try {
    const id = req.params.id;
    const fighter = FighterService.delete(id);
    if (fighter[0]) {
      let { name } = fighter[0];
      res.send(name + " delete successful");
    } else throw new Error("Fighter not deleted");
  } catch (e) {
    res.send(e.message);
  }
});

module.exports = router;
