const {
  updateUserValid,
  createUserValid,
} = require("../middlewares/user.validation.middleware");
const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAll() {
    const result = UserRepository.getAll();
    if (result) {
      return result;
    } else console.log("error getAll users");
  }

  getOne(id) {
    const result = this.search(id);
    if (result) {
      return result;
    } else console.log("error getOne user");
  }

  createUser(user) {
    const result = UserRepository.create(user);
    if (result) {
      return result;
    } else console.log("error create user");
  }

  updateUser(id, dataToUpdate) {
    const result = UserRepository.update(id, dataToUpdate);
    if (result) return result;
    else console.log("error update user");
  }

  delete(id) {
    const result = UserRepository.delete(id);
    if (result) {
      return result;
    } else console.log("error delete  user");
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
