const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAll() {
    const result = FighterRepository.getAll();
    if (result) {
      return result;
    } else console.log("error getAll fighter");
  }
  getOne(id) {
    const result = this.search(id);
    if (result) {
      return result;
    } else console.log("error getOne fighter");
  }
  createUser(user) {
    const result = FighterRepository.create(user);
    if (result) {
      return result;
    } else console.log("error create fighter");
  }
  updateUser(id, dataToUpdate) {
    const result = FighterRepository.update(id, dataToUpdate);
    if (result) return result;
    else console.log("error update fighter");
  }
  delete(id) {
    const result = FighterRepository.delete(id);
    if (result) {
      return result;
    } else console.log("error delete  fighter");
  }
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
